package com.sber.lecture24_springmvc.controller;

import com.sber.lecture24_springmvc.payload.PostCreateRequest;
import com.sber.lecture24_springmvc.payload.PostResponse;
import com.sber.lecture24_springmvc.services.PostService;
import com.sber.lecture24_springmvc.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class PostController {

    private final PostService postService;

    @GetMapping("/all-posts")
    public List<PostResponse> findAllSort(){
        return postService.findAllSort();
    }

    @PostMapping("/write-new-post")
    public ResponseEntity<PostResponse> writeNewPost(@RequestBody PostCreateRequest post){
        PostResponse postResponse = postService.savePost(post);
        return new ResponseEntity<>(postResponse, HttpStatus.CREATED);
    }
}
