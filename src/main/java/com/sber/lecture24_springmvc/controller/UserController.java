package com.sber.lecture24_springmvc.controller;

import com.sber.lecture24_springmvc.payload.UserResponse;
import com.sber.lecture24_springmvc.payload.UserSaveRequest;
import com.sber.lecture24_springmvc.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/save-new-user")
    public ResponseEntity<UserResponse> registration(@Valid @RequestBody UserSaveRequest userSaveRequest){
        UserResponse userResponse = userService.saveUser(userSaveRequest);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }


}
