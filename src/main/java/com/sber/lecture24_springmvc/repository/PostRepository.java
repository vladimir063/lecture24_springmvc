package com.sber.lecture24_springmvc.repository;

import com.sber.lecture24_springmvc.entity.Post;
import com.sber.lecture24_springmvc.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

     List<Post> findAllByOrderByTime();
}
