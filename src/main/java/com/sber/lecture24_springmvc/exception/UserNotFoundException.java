package com.sber.lecture24_springmvc.exception;

public class UserNotFoundException extends RuntimeException  {

    public UserNotFoundException(String message) {
        super(message);
    }
}
