package com.sber.lecture24_springmvc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostResponse {


    private Long id;

    private String text;

    private String nickname;

    private Integer age;

    private LocalDateTime time;

}
