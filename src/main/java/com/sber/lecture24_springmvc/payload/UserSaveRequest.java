package com.sber.lecture24_springmvc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSaveRequest {

    @NotBlank(message = "Введите ник")
    private String nickname;

    @Min(value = 10,message = "Минимальный возраст регистарции 16")
    private Integer age;

}
