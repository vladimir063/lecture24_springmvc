package com.sber.lecture24_springmvc.services;

import com.sber.lecture24_springmvc.entity.User;
import com.sber.lecture24_springmvc.payload.UserResponse;
import com.sber.lecture24_springmvc.payload.UserSaveRequest;

import java.util.List;

public interface UserService {

    List<User> findAll();

    UserResponse saveUser(UserSaveRequest userSaveRequest);
}
