package com.sber.lecture24_springmvc.services;

import com.sber.lecture24_springmvc.payload.PostCreateRequest;
import com.sber.lecture24_springmvc.payload.PostResponse;

import java.util.List;

public interface PostService {

    List<PostResponse> findAllSort();

    PostResponse savePost(PostCreateRequest post);

    PostResponse findById(Long id);
}
