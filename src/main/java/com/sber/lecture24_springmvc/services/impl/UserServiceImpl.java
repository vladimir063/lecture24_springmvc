package com.sber.lecture24_springmvc.services.impl;

import com.sber.lecture24_springmvc.entity.User;
import com.sber.lecture24_springmvc.payload.UserResponse;
import com.sber.lecture24_springmvc.payload.UserSaveRequest;
import com.sber.lecture24_springmvc.repository.UserRepository;
import com.sber.lecture24_springmvc.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserResponse saveUser(UserSaveRequest userSaveRequest) {
        User user = new User();
        user.setNickname(userSaveRequest.getNickname());
        user.setAge(userSaveRequest.getAge());
        User userSave = userRepository.save(user);
        return new UserResponse(user.getId(), userSave.getNickname(),  userSave.getAge());
    }
}
