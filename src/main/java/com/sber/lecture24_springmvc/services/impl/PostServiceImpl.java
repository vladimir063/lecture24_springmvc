package com.sber.lecture24_springmvc.services.impl;


import com.sber.lecture24_springmvc.entity.Post;
import com.sber.lecture24_springmvc.entity.User;
import com.sber.lecture24_springmvc.exception.UserNotFoundException;
import com.sber.lecture24_springmvc.payload.PostCreateRequest;
import com.sber.lecture24_springmvc.repository.PostRepository;
import com.sber.lecture24_springmvc.payload.PostResponse;
import com.sber.lecture24_springmvc.repository.UserRepository;
import com.sber.lecture24_springmvc.services.PostService;
import com.sber.lecture24_springmvc.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Override
    public List<PostResponse> findAllSort() {
        List<Post> posts = postRepository.findAllByOrderByTime();
        List<PostResponse> postsResponse = new ArrayList<>();
        posts.forEach(post -> postsResponse.add(new PostResponse(
                post.getId(), post.getText(), post.getUser().getNickname(), post.getUser().getAge(), post.getTime())));
        return postsResponse;
    }

    @Override
    public PostResponse savePost(PostCreateRequest postCreateRequest) {
        Post post = new Post();
        User user = userRepository.findById(postCreateRequest.getUserId())
                .orElseThrow(() -> new UserNotFoundException("Пользователь с id " + postCreateRequest.getUserId() + " не найден"));
        post.setText(postCreateRequest.getText());
        post.setUser(user);
        post.setTime(LocalDateTime.now());
        Post postSave = postRepository.save(post);
        return new PostResponse(postSave.getId(), postSave.getText(), postSave.getUser().getNickname(), postSave.getUser().getAge(), post.getTime());
    }

    @Override
    public PostResponse findById(Long id) {
        Post post = postRepository.findById(id).orElseThrow();
        return new PostResponse(post.getId(), post.getText(), post.getUser().getNickname(), post.getUser().getAge(), post.getTime());
    }
}
