package com.sber.lecture24_springmvc.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.sber.lecture24_springmvc.entity.Post;
import com.sber.lecture24_springmvc.entity.User;
import com.sber.lecture24_springmvc.payload.PostCreateRequest;
import com.sber.lecture24_springmvc.repository.PostRepository;
import com.sber.lecture24_springmvc.repository.UserRepository;
import com.sber.lecture24_springmvc.services.PostService;
import com.sber.lecture24_springmvc.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;



@SpringBootTest
class PostControllerIT {

    private MockMvc mockMvc;
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    private ObjectMapper objectMapper;

    private User user;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new PostController(postService)).build();
        user = new User();
        user.setNickname("Bob");
        user.setAge(25);
        user = userRepository.save(user);
    }

    @Test
    void findAllSort() throws Exception {
        Post post1 = new Post();
        post1.setText("Where?");
        post1.setUser(user);
        post1 = postRepository.save(post1);

        Post post2 = new Post();
        post2.setText("Yes");
        post2.setUser(user);
        post2 = postRepository.save(post2);

        mockMvc.perform(get("/all-posts")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text", is("Where?")))
                .andExpect(jsonPath("$[1].text", is("Yes")));
    }

    @Test
    void writeNewPost() throws Exception {

        PostCreateRequest postCreateRequest = new PostCreateRequest("Hello", user.getId());
        mockMvc.perform(post("/write-new-post")
                        .content(objectMapper.writeValueAsString(postCreateRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.text").value("Hello"))
                .andExpect(jsonPath("$.nickname").value("Bob"))
                .andExpect(jsonPath("$.age").value("25"))
                .andExpect(status().isCreated());
    }
}