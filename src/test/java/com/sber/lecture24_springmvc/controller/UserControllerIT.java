package com.sber.lecture24_springmvc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sber.lecture24_springmvc.payload.UserSaveRequest;
import com.sber.lecture24_springmvc.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
class UserControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService)).build();
    }

    @Test
    void registration() throws Exception {
        UserSaveRequest userSaveRequest = new UserSaveRequest("Mark", 30);
        mockMvc.perform(post("/save-new-user")
                        .content(objectMapper.writeValueAsString(userSaveRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.nickname").value("Mark"))
                .andExpect(jsonPath("$.age").value("30"))
                .andExpect(status().isCreated());
    }
}